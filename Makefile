BOOK_NAME = samplebook

.PHONY: install
install:
	docker-compose pull && docker-compose build

.PHONY: init
init:
	docker-compose run review review-init ${BOOK_NAME}
	cp {Dockerfile,docker-compose.yml,Makefile} ${BOOK_NAME}/

.PHONY: build_pdf
build_pdf:
	docker-compose run review rake pdf
